# LaravelCredito

- Se crea web service con laravel
- Para instalarlo se debe descargar del repositorio https://bitbucket.org/somarasiel/laravelcredito.git  
- Una vez clonado nos ubicamos dentro del proyecto y se ejecuta el comando composer install para instalar las dependencias 
- Cuando ya se instalen las dependencias subimos el servidor con php artisan serve

Web Service
- Se realizan las respectivas migraciones del las tablas Clientes Creditos y Abonos
- se crean las relaciones en los modelos de laravel para las tablas clientes Creditos y Abonos
- se crean servicios web para CRUD de todos los controladores Clientes Creditos y Abonos
- se pueden observar las rutas del servicio web con el comando php artisan route:list

- las peticiones se realizan a las urls 
    - http://127.0.0.1:8000/api/clientes
    - http://127.0.0.1:8000/api/creditos
    - http://127.0.0.1:8000/api/abonos
