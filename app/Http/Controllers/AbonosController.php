<?php

namespace App\Http\Controllers;

use App\abonos;
use Illuminate\Http\Request;

class AbonosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        echo json_encode(abonos::get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $abono = new abonos();
       
        $abono->id_abono = $request->input('id_abono');
        $abono->creditos_id_credito = $request->input('creditos_id_credito');
        $abono->valor_abono = $request->input('valor_abono');
        $abono->abono_capital = $request->input('abono_capital');
        $abono->intereses = $request->input('intereses');
        $abono->saldo = $request->input('saldo');
        $abono->fecha_abono = $request->input('fecha_abono');
        $abono->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\abonos  $abonos
     * @return \Illuminate\Http\Response
     */
    public function show( $abonos)
    {
        echo json_encode(abonos::find($abonos));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\abonos  $abonos
     * @return \Illuminate\Http\Response
     */
    public function edit(abonos $abonos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\abonos  $abonos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $abonos)
    {
        $abono = abonos::find($abonos);
       
        $abono->id_abono = $request->input('id_abono');
        $abono->creditos_id_credito = $request->input('creditos_id_credito');
        $abono->valor_abono = $request->input('valor_abono');
        $abono->abono_capital = $request->input('abono_capital');
        $abono->intereses = $request->input('intereses');
        $abono->saldo = $request->input('saldo');
        $abono->fecha_abono = $request->input('fecha_abono');
        $abono->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\abonos  $abonos
     * @return \Illuminate\Http\Response
     */
    public function destroy( $abonos)
    {
        $abono = abonos::find($abonos);
        $abono->delete();
    }
}
