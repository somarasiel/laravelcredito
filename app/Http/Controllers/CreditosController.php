<?php

namespace App\Http\Controllers;

use App\creditos;
use Illuminate\Http\Request;

class CreditosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        echo json_encode(creditos::get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $credito = new creditos();
       
        $credito->id_credito = $request->input('id_credito');
        $credito->clientes_documento = $request->input('clientes_documento');
        $credito->valor_credito = $request->input('valor_credito');
        $credito->fecha_desembolso = $request->input('fecha_desembolso');
        $credito->save();
    }
    

    /**
     * Display the specified resource.
     *
     * @param  \App\creditos  $creditos
     * @return \Illuminate\Http\Response
     */
    public function show( $creditos)
    {
        echo json_encode(creditos::find($creditos));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\creditos  $creditos
     * @return \Illuminate\Http\Response
     */
    public function edit(creditos $creditos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\creditos  $creditos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $creditos)
    {
        $credito = creditos::find($creditos);
       
        $credito->id_credito = $request->input('id_credito');
        $credito->clientes_documento = $request->input('clientes_documento');
        $credito->valor_credito = $request->input('valor_credito');
        $credito->fecha_desembolso = $request->input('fecha_desembolso');
        $credito->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\creditos  $creditos
     * @return \Illuminate\Http\Response
     */
    public function destroy(creditos $creditos)
    {
        $credito = creditos::find($creditos);
        $credito->delete();
    }
}
