<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class creditos extends Model
{
    public function clientes(){
        return $this->hasOne(clientes::class);
    }
    public function abonos()
    {
        return $this->hasMany(abonos::class);
    }
}
