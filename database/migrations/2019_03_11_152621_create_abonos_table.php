<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAbonosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('abonos', function (Blueprint $table) {

            $table->integer('id_abono')->unsigned()->primary();
            $table->integer('creditos_id_credito')->unsigned();
            $table->bigInteger('valor_abono')->unsigned();
            $table->bigInteger('abono_capital')->unsigned();
            $table->bigInteger('intereses')->unsigned();
            $table->bigInteger('saldo')->unsigned();
            $table->date('fecha_abono')->nullable();
            $table->foreign('creditos_id_credito')->references('id_credito')->on('creditos');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('abonos');
    }
}
