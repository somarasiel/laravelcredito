<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('creditos', function (Blueprint $table) {
            $table->integer('id_credito')->unsigned()->primary();
            $table->integer('clientes_documento')->unsigned();
            $table->bigInteger('valor_credito')->unsigned();
            $table->date('fecha_desembolso')->nullable();
            $table->timestamps();
            $table->foreign('clientes_documento')->references('documento')->on('clientes');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('creditos');
    }
}
